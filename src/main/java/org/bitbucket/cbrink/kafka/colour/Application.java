package org.bitbucket.cbrink.kafka.colour;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Arrays;
import java.util.Properties;

@Slf4j
public class Application {
    private static final String[] VALID_COLOURS = new String[] {"green", "red", "blue"};
    public static void main(String[] args) {
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "favourite-colour");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

//        config.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> textLines = builder.stream("user-colour-input");
        KStream<String, String> userAndColour = textLines.filter(((key, value) -> value.contains(",")))
                .selectKey((key, value) -> value.split(",")[0].toLowerCase())
                .mapValues(value -> value.split(",")[1].toLowerCase())
                .filter((user, colour) -> Arrays.asList(VALID_COLOURS).contains(colour));

        userAndColour.to("user-keys-and-colours", Produced.with(Serdes.String(), Serdes.String()));

        KTable<String, String> userFavColoursTable = builder.table("user-keys-and-colours");
        KTable<String, Long> favoritesColours= userFavColoursTable.groupBy((user, colour) -> new KeyValue<>(colour, colour))
                .count(Materialized.as("countsByColours").with(Serdes.String(), Serdes.Long()));
        favoritesColours.toStream().to("fav-colour-output", Produced.with(Serdes.String(), Serdes.Long()));

        Topology topology = builder.build();
        KafkaStreams streams = new KafkaStreams(topology, config);
//        streams.cleanUp();
        streams.start();
        log.info("STREAM: {}", streams.localThreadsMetadata().toString());

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }
}

